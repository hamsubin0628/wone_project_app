import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_form_validator.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/join_membership_page.dart';
import 'package:wone_app/pages/page_index.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({super.key});

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    final _formKey = GlobalKey<FormState>();

    return SingleChildScrollView(
      child: // 로그인 화면
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        // alignment: FractionalOffset.center,
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // 메인 로고
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 25, 15),
              child: Image.asset(
                '${mainLogo}',
                width: mediaQueryWidth * 0.35,
                height: mediaQueryHeight * 0.35,
              ),
            ),

            // 입력 폼
            Container(
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    // 아이디 입력 창
                    Container(
                      margin: edgeInsetsLogIn,
                      width: mediaQueryWidth * widthLogIn,
                      // height: 40,
                      child: TextFormField(

                        // 키보드 기본
                        keyboardType: TextInputType.text,

                        // 글자 수 제한
                        maxLength: 15,

                        // 유효성 검사
                        validator: (value) {
                          if (value!.isEmpty) {
                            return formErrorRequired;
                          }
                          if (value.toString().length >= 15) {
                            return formErrorMaxLength(15);
                          }
                          if (!RegExp('[a-z A-Z 0-9：]').hasMatch(value)) {
                            return formErrorId;
                          }
                        },

                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey.withOpacity(0.5)
                            ),
                            borderRadius: BorderRadius.circular(buttonRadius),
                          ),
                          contentPadding: EdgeInsets.only(left: 15), // 텍스프 필드 내부 패딩
                          counterText: '', // counter text를 비움으로 설정
                          hintText: '아이디',
                          hintStyle: TextStyle(
                            letterSpacing: fontLetterSpacing,
                            color: colorGrey,
                          ),
                          errorStyle: TextStyle(
                            fontSize: 12,
                            letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ),

                    // 비밀번호 입력 창
                    Container(
                      margin: edgeInsetsLogIn,
                      width: mediaQueryWidth * widthLogIn,
                        // height: 40,
                      child: TextFormField(
                        // 키보드 기본
                        keyboardType: TextInputType.text,

                        //비밀번호 안 보이게 함
                        obscureText: true,

                        // 글자 수 제한
                        maxLength: 15,

                        // 유효성 검사
                        validator: (value) {
                          if (value!.isEmpty) {
                            return formErrorRequired;
                          }
                          if (value.toString().length >= 20) {
                            return formErrorMaxLength(20);
                          }
                          if (!RegExp('[a-z A-Z 0-9：]').hasMatch(value)) {
                            return formErrorId;
                          }
                        },

                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(buttonRadius),
                          ),
                          contentPadding: EdgeInsets.only(left: 15), // 텍스프 필드 내부 패딩
                          counterText: '', // counter text를 비움으로 설정
                          hintText: '비밀번호',
                          hintStyle: TextStyle(
                            letterSpacing: fontLetterSpacing,
                            color: colorGrey,
                          ),
                          errorStyle: TextStyle(
                              fontSize: 12,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // 로그인 버튼
            Container(
              margin: edgeInsetsLogIn,
              width: mediaQueryWidth * widthLogIn,
              height: 45,
              child: ElevatedButton(
                  style: OutlinedButton.styleFrom(
                    backgroundColor: colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(buttonRadius),
                    ),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageIndex()));
                    }
                    //Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageIndex()));
                  },
                  child: const Text(
                    '로그인',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_Bold',
                      color: colorInsideFont
                    ),
                  )
              ),
            ),

            // 회원가입 이동 버튼
            Container(
              margin: edgeInsetsLogIn,
              width: mediaQueryWidth * widthLogIn,
              height: 45,
              child: ElevatedButton(
                  style: OutlinedButton.styleFrom(
                      backgroundColor: colorLightGrey,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(buttonRadius),
                      )
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => JoinMembershipPage()));
                  },
                  child: Text(
                    '회원가입',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_Bold',
                      color: colorPrimary
                    ),
                  )
              ),
            ),
          ],
        ),
      )
    );
  }
}
