import 'package:pattern_formatter/date_formatter.dart';
import 'package:intl/intl.dart';

class MemberDetailItem {
  String username;
  DateTime birthDate;
  String password;
  String cardGroup;
  String cardNumber;
  DateTime endDate;
  String etcMemo;

  MemberDetailItem(this.username, this.birthDate, this.password, this.cardGroup, this.cardNumber, this.endDate, this.etcMemo);

  factory MemberDetailItem.fromJson(Map<String, dynamic> json) {
    DateTime birthDate = DateTime.parse(json['birth_date']);
    String birthDateString = DateFormat('yyyy-MM-DD').format(birthDate);
    print(birthDateString);

    return MemberDetailItem(
        json['username'],
        json['birthDate'],
        json['password'],
        json['cardGroup'],
        json['cardNumber'],
        json['endDate'],
        json['etcMemo']
    );
  }
}