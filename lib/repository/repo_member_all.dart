import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/member_detail_item.dart';

class RepoMemberAll {
  Future<MemberAllResult> getMemberAlls() async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/member/all';

    final response = await dio.get(_baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }));
    return MemberAllResult.fromJson(response.data);
  }
}