import 'package:flutter/material.dart';
import 'package:wone_app/model/member_detail_item.dart';

class ComponentMemberAll extends StatelessWidget {
  const ComponentMemberAll({
    super.key,
    required this.memberAll,
    required this.callback

  });
  final MemberAll memberAll;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Text(
            '${memberAll.userId}',
          ),
          Text(
            memberAll.username,
          ),
          Text(
            '${memberAll.joinDate}'
          ),
          Text(
            memberAll.memberGroupType
          ),
        ],
      ),
    );
  }
}
